package com.huawei.cse.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2019-02-27T01:36:18.418Z")

@RestSchema(schemaId = "projectx1cy")
@RequestMapping(path = "/cse", produces = MediaType.APPLICATION_JSON)
public class Projectx1cyImpl {

    @Autowired
    private Projectx1cyDelegate userProjectx1cyDelegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userProjectx1cyDelegate.helloworld(name);
    }

}
