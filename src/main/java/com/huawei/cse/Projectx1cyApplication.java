package com.huawei.cse;

import org.apache.servicecomb.springboot.starter.provider.EnableServiceComb;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableServiceComb
public class Projectx1cyApplication {
    public static void main(String[] args) {
         SpringApplication.run(Projectx1cyApplication.class,args);
    }
}
